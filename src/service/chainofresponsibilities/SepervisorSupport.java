/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.chainofresponsibilities;

/**
 *
 * @author nidhal.bentahar
 */
public class SepervisorSupport implements ISupportService {

  private ISupportService next = null;

  public ISupportService getNext() {
    return next;
  }

  public void setNext(ISupportService next) {
    this.next = next;
  }

  @Override
  public void handleRequest(SupportRequest request) {
    if (request.getType() == ServiceLevel.LEVEL_TWO) {
      request.setConclusion("Supervisor solved the problem.. You are a hero.");
    } else {
      if (next != null) {
        next.handleRequest(request);
      } else {
        throw new IllegalArgumentException("No handler found for :: " + request.getType());
      }
    }
  }

}
