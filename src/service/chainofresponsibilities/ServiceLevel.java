/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.chainofresponsibilities;

/**
 *
 * @author nidhal.bentahar
 */
public enum ServiceLevel {

  LEVEL_ONE, LEVEL_TWO, LEVEL_THREE, LEVEL_FOUR, INVALIDREQUEST
}
