/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.chainofresponsibilities;

/**
 *
 * @author nidhal.bentahar
 */
public class SupportService implements ISupportService {

  private ISupportService handler = null;

  @Override
  public void handleRequest(SupportRequest request) {
    handler.handleRequest(request);
  }

  public ISupportService getHandler() {
    return handler;
  }

  public void setHandler(ISupportService handler) {
    this.handler = handler;
  }

}
