/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.chainofresponsibilities;

/**
 *
 * @author nidhal.bentahar
 */
public class SupportRequest {

  private ServiceLevel type;
  private String conclusion = null;

  public ServiceLevel getType() {
    return type;
  }

  public void setType(ServiceLevel type) {
    this.type = type;
  }

  public String getConclusion() {
    return conclusion;
  }

  public void setConclusion(String conclusion) {
    this.conclusion = conclusion;
  }

}
