/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log.proxy;

/**
 *
 * @author nidhal.bentahar
 */
public class CachedLogger implements ICachedLogging {

  @Override
  public void logRequest(String logString) {
    System.out.println("Cached logger, it's expesive to log " + logString + " But logging anyway");
  }

}
