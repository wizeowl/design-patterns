/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log.proxy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class CachedLoggingProxy implements ICachedLogging {

  protected List<String> cachedLogEntries = new ArrayList<String>();
  protected CachedLogger logger = new CachedLogger();

  @Override
  public void logRequest(String logString) {
    addLogRequest(logString);
  }

  private void addLogRequest(String logString) {
    cachedLogEntries.add(logString);
    if (cachedLogEntries.size() > 4) {
      performLogging();
    }
  }

  private void performLogging() {
    StringBuffer cumul = new StringBuffer();
    for (String s : cachedLogEntries) {
      cumul.append("\n" + s);
    }
    logger.logRequest(cumul.toString());
    cachedLogEntries.clear();
  }

}
