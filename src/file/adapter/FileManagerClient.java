/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.adapter;

/**
 *
 * @author nidhal.bentahar
 */
public class FileManagerClient {

  public static void main(String[] args) {
    FileManager f = null;
    String dummyData = "dynamite";
    f = new FileManagerImpl();
    System.out.println("Using file manager:" + f.getClass().toString());
    f.open("dummyfile.txt");
    f.write(0, dummyData.length(), dummyData.getBytes());
    String test = f.read(0, dummyData.length(), dummyData.getBytes());
    System.out.println("Data written and read: " + test);
    f.close();
  }

}
