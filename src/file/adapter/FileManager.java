/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.adapter;

/**
 *
 * @author nidhal.bentahar
 */
public interface FileManager {

  public String open(String s);

  public String close();

  public String read(int pos, int amount, byte[] data);

  public String write(int pos, int amount, byte[] data);
}
