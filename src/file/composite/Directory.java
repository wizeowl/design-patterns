/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class Directory implements AbstractFile {

  private String name;
  private List<AbstractFile> files = new ArrayList<AbstractFile>();
  private Indentation indentation;

  public Directory(String name, Indentation indentation) {
    this.name = name;
    this.indentation = indentation;
  }

  public void add(AbstractFile f) {
    files.add(f);
  }

  @Override
  public void ls() {
    System.out.println(indentation.getIndentation() + name);
    indentation.increaseIndentation();
    for (AbstractFile f : files) {
      f.ls();
    }
    indentation.decreaseIndentation();
  }

}
