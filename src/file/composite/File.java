/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.composite;

/**
 *
 * @author nidhal.bentahar
 */
public class File implements AbstractFile {

  private String name;
  private Indentation indentation;

  public File(String name, Indentation indentation) {
    this.name = name;
    this.indentation = indentation;
  }

  @Override
  public void ls() {
    System.out.println(indentation.getIndentation() + name);
  }

}
