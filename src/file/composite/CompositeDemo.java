/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.composite;

/**
 *
 * @author nidhal.bentahar
 */
public class CompositeDemo {

  public static void main(String[] args) {
    Indentation indentation = new Indentation();
    Directory dir1 = new Directory("dir111", indentation);
    Directory dir2 = new Directory("dir222", indentation);
    Directory dir3 = new Directory("dir333", indentation);
    File f1 = new File("a1", indentation);
    File f2 = new File("a2", indentation);
    File f3 = new File("a3", indentation);
    File f4 = new File("a4", indentation);
    dir1.add(f1);
    dir1.add(dir2);
    dir2.add(f2);
    dir2.add(f3);
    dir2.add(dir3);
    dir3.add(f4);
    dir1.ls();
  }
}
