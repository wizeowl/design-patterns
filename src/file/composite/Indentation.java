/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file.composite;

/**
 *
 * @author nidhal.bentahar
 */
public class Indentation {

  private StringBuffer sbIndent = new StringBuffer();

  String getIndentation() {
    return sbIndent.toString();
  }

  void decreaseIndentation() {
    if (sbIndent.length() >= 4) {
      sbIndent.setLength(sbIndent.length() - 4);
    }
  }

  void increaseIndentation() {
    sbIndent.append("    ");
  }

}
