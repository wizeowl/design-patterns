/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package event.observer;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author nidhal.bentahar
 */
public class ResponseHandler implements Observer {

  private String response;

  @Override
  public void update(Observable o, Object arg) {
    if (arg instanceof String) {
      response = (String) arg;
      System.out.println("\nReceived Response: " + response);
    }
  }

}
