/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.facade;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author nidhal.bentahar
 */
public class TravelFacade {

  private HotelBooker hotelBooker;
  private FlightBooker flightBooker;

  public void getBookings(Date from, Date to) {
    ArrayList<Flight> flights = flightBooker.getFlightsFor(from, to);
    ArrayList<Hotel> hotels = hotelBooker.getHotelNamesFor(from, to);
    // Add processing here
  }
}
