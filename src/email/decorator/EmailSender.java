/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class EmailSender {

  public void sendEmail(IEmail email) {
    ExternalEmailDecorator external = new ExternalEmailDecorator(email);
    SecureEmailDecorator secure = new SecureEmailDecorator(external);
    send(secure.getContents());
  }

  private void send(String email) {
    System.out.println(email);
    System.out.println("Thanna! Messagek wsol.");
  }
}
