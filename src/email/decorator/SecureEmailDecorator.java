/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class SecureEmailDecorator extends EmailDecorator {

  String content;

  public SecureEmailDecorator(IEmail basicEmail) {
    originalEmail = basicEmail;
  }

  @Override
  public String getContents() {
    content = encrypt(originalEmail.getContents());
    return content;
  }

  public String encrypt(String message) {
    return message + "\n Habrakadabra, it's now encrypted!!";
  }

}
