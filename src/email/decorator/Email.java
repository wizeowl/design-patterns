/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Email implements IEmail {

  private String content;

  public Email(String content) {
    this.content = content;
  }

  @Override
  public String getContents() {
    return content;
  }

}
