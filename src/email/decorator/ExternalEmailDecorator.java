/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class ExternalEmailDecorator extends EmailDecorator {

  private String content;

  public ExternalEmailDecorator(IEmail basicEmail) {
    originalEmail = basicEmail;
  }

  @Override
  public String getContents() {
    content = addDisclaimer(originalEmail.getContents());
    return content;
  }

  private String addDisclaimer(String contents) {
    return contents + "\n Company Disclaimer";
  }

}
