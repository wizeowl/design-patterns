/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tv.state;

/**
 *
 * @author nidhal.bentahar
 */
public class TVContext implements State {

  private State state;

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  @Override
  public void doAction() {
    this.state.doAction();
  }

}
