/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tv.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class RemoteControl {

  private TV implementor;

  public void on() {
    implementor.on();
  }

  public void off() {
    implementor.off();
  }

  public void setChannel(int channel) {
    implementor.tuneChannel(channel);
  }
}
