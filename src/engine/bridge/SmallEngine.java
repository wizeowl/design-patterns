/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class SmallEngine implements Engine {

  int horsePower;

  public SmallEngine() {
    horsePower = 100;
  }

  @Override
  public int go() {
    System.out.println("vrum vrum..");
    return horsePower;
  }

}
