/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class Bridge {

  public static void main(String[] args) {
    Vehicle bus = new BigBus(new SmallEngine());
    bus.drive();
    bus.setEngine(new BigEngine());
    bus.drive();

    Vehicle car = new SmallCar(new SmallEngine());
    car.drive();
    car.setEngine(new BigEngine());
    car.drive();
  }
}
