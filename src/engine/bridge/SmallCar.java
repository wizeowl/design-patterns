/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class SmallCar extends Vehicle {

  public SmallCar(Engine engine) {
    this.engine = engine;
    this.weightInKilos = 600;
  }

  @Override
  public void drive() {
    System.out.println("3iiin");
    int horsePower = engine.go();
    reportOnSpeed(horsePower);
  }

}
