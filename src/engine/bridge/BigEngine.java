/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class BigEngine implements Engine {

  int horsePower;

  public BigEngine() {
    horsePower = 350;
  }

  @Override
  public int go() {
    System.out.println("VROOM VROMM");
    return horsePower;
  }

}
