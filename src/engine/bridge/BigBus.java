/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package engine.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class BigBus extends Vehicle {

  public BigBus(Engine engine) {
    this.weightInKilos = 2000;
    this.engine = engine;
  }

  @Override
  public void drive() {
    System.out.println("3aaan");
    int horsePower = engine.go();
    reportOnSpeed(horsePower);
  }

}
