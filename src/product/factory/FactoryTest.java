/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package product.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class FactoryTest {
  public static void main(String[] args) {
    ProductFactory pf = new ProductFactory();
    Product prod;
    prod = pf.createProduct("A");
    prod.writeName("ELLEMBI");
    prod = pf.createProduct("B");
    prod.writeName("ELLEMBI");
  }
}
