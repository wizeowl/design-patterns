/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package product.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class ProductFactory {
  Product createProduct(String type){
    if(type == "B"){
      return new ProductB();
    }
    return new ProductA();
  }
}
