package product.factory;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class Product {

  public void writeName(String name) {
    System.out.println("My name is " + name);
  }
}
