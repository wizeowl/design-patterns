/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package product.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class ProductB extends Product{
  @Override
  public void writeName(String name){
    StringBuilder tempName = new StringBuilder().append(name);
    System.out.println("My name is reversed " + tempName.reverse());
  }
}
