/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product.prototype;

import java.util.Hashtable;

/**
 *
 * @author nidhal.bentahar
 */
public class PrototypeExample {

  Hashtable<String, Product> productMap = new Hashtable<String, Product>();

  public Product getProduct(String productCode) {
    Product cachedProduct = (Product) productMap.get(productCode);
    return (Product) cachedProduct.clone();
  }

  public void loadCache() {
    Book b1 = new Book();
    b1.setDescription("El ketni yete7adda!");
    b1.setSKU("B1");
    b1.setNumberOfPages(100);
    productMap.put(b1.getSKU(), b1);
    DVD d1 = new DVD();
    d1.setDescription("SuperFitouri");
    d1.setSKU("D1");
    d1.setDuration(180);
    productMap.put(d1.getSKU(), d1);
  }

  public static void main(String[] args) {
    PrototypeExample pe = new PrototypeExample();
    pe.loadCache();
    Book clonedBook = (Book) pe.getProduct("B1");
    System.out.println(clonedBook.getDescription());
    System.out.println(clonedBook.getSKU());
    System.out.println(clonedBook.getNumberOfPages());
    DVD clonedDVD = (DVD) pe.getProduct("D1");
    System.out.println(clonedDVD.getDescription());
    System.out.println(clonedDVD.getSKU());
    System.out.println(clonedDVD.getDuration());
  }
}
