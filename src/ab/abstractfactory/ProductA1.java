/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class ProductA1 extends AbstractProductA {

  public ProductA1(String arg) {
    System.out.println("Helllo " + arg);
  }

  @Override
  public void operationA1() {

  }

  @Override
  public void operationA2() {

  }

}
