/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class Client {
  public static void main(String[] args) {
    AbstractFactory pf = FactoryMaker.getFactory(Factories.ONE);
    AbstractProductA a = pf.createProductA();
  }
}
