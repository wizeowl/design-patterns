/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class FactoryMaker {

  private static AbstractFactory pf = null;

  public static AbstractFactory getFactory(Factories factory) {
    if (factory == Factories.ONE) {
      pf = new ConcreteFactory1();
    } else if (factory == Factories.TWO) {
      pf = new ConcreteFactory2();
    }
    return pf;
  }
}
