/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class ProductB1 extends AbstractProductB {

  public ProductB1(String arg) {
    System.out.println("Yo Yo " + arg);
  }

  @Override
  public void operationB1() {

  }

  @Override
  public void operationB2() {

  }

}
