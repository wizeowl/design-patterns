/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class AbstractProductB {
  public abstract void operationB1();
  public abstract void operationB2();
}
