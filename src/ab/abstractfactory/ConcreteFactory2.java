/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ab.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class ConcreteFactory2 extends AbstractFactory {

  @Override
  AbstractProductA createProductA() {
    return new ProductA2("3laya");
  }

  @Override
  AbstractProductB createProductB() {
    return new ProductB2("Ena");
  }

}
