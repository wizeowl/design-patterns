/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public class ConcreteUser extends User {

  public ConcreteUser(ChatMediator mediator, String name) {
    super(mediator, name);
  }

  @Override
  public void send(String message) {
    System.out.println(this.name + " is sending: " + message);
    mediator.sendMessage(message, this);
  }

  @Override
  public void receive(String message) {
    System.out.println(this.name + " is receiving: " + message);
  }

}
