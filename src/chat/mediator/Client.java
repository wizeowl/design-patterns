/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chat.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public class Client {

  public static void main(String[] args) {
    ChatMediator mediator = new ConcreteChatMediator();
    User user1 = new ConcreteUser(mediator, "Panenka");
    User user2 = new ConcreteUser(mediator, "Yachin");
    User user3 = new ConcreteUser(mediator, "Gulit");
    User user4 = new ConcreteUser(mediator, "Timotchuk");
    mediator.addUser(user1);
    mediator.addUser(user2);
    mediator.addUser(user3);
    mediator.addUser(user4);
    user1.send("Hi!!!!");
  }
}
