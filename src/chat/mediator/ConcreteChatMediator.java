/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.mediator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class ConcreteChatMediator implements ChatMediator {

  private List<User> users;

  public ConcreteChatMediator() {
    users = new ArrayList<User>();
  }

  @Override
  public void sendMessage(String message, User user) {
    for (User u : users) {
      if (u != user) {
        u.receive(message);
      }
    }
  }

  @Override
  public void addUser(User user) {
    this.users.add(user);
  }

}
