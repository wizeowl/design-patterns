/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package house.bulder;

/**
 *
 * @author nidhal.bentahar
 */
public class HouseClient {
  public static void main(String[] args) {
    HouseDirector director = new HouseDirector();
    HouseBuilder builder = new WoodBuilder();
    House woodHouse = director.constructHouse(builder);
  }
}
