/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package house.bulder;

/**
 *
 * @author nidhal.bentahar
 */
public class HouseDirector {

  public House constructHouse(HouseBuilder builder) {
    House house = builder.createHouse();
    System.out.println(house.getRepresentation());
    house.setFloor(builder.createFloor());
    System.out.println(house.getFloor().getRepresentation());
    house.setWalls(builder.createWalls());
    System.out.println(house.getWalls().getRepresentation());
    house.setRoof(builder.createRoof());
    System.out.println(house.getRoof().getRepresentation());
    return house;
  }
}
