/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package house.bulder;

/**
 *
 * @author nidhal.bentahar
 */
public class WoodBuilder extends HouseBuilder {

  @Override
  public House createHouse() {
    house = new WoodHouse();
    return house;
  }

  @Override
  public Floor createFloor() {
    floor = new WoodFloor();
    return floor;
  }

  @Override
  public Walls createWalls() {
    walls = new WoodWalls();
    return walls;
  }

  @Override
  public Roof createRoof() {
    roof = new WoodRoof();
    return roof;
  }

}
