/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package house.bulder;

/**
 *
 * @author nidhal.bentahar
 */
public class BrickBuilder extends HouseBuilder {

  @Override
  public House createHouse() {
    house = new BrickHouse();
    return house;
  }

  @Override
  public Floor createFloor() {
    floor = new BrickFloor();
    return floor;
  }

  @Override
  public Walls createWalls() {
    walls = new BrickWalls();
    return walls;
  }

  @Override
  public Roof createRoof() {
    roof = new BrickRoof();
    return roof;
  }

}
