/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pure.memento;

/**
 *
 * @author nidhal.bentahar
 */
public class Memento {

  private String state;

  public Memento(String state) {
    this.state = state;
  }

  String getSavedState() {
    return state;
  }

}
