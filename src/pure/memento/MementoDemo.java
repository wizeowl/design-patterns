/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pure.memento;

/**
 *
 * @author nidhal.bentahar
 */
public class MementoDemo {

  public static void main(String[] args) {
    Caretaker c = new Caretaker();
    Originator o = new Originator();
    o.setState("State1");
    o.setState("State2");
    c.addMemento(o.saveToMemento());
    o.setState("State3");
    c.addMemento(o.saveToMemento());
    o.setState("State4");
    o.restore(c.getMemento(0));
  }
  
}
