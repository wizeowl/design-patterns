/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pure.memento;

/**
 *
 * @author nidhal.bentahar
 */
public class Originator {

  private String state;

  public void setState(String state) {
    this.state = state;
  }

  public Object saveToMemento() {
    System.out.println("Saving");
    return new Memento(state);
  }

  public void restore(Object m) {
    if (m instanceof Memento) {
      Memento memento = (Memento) m;
      state = memento.getSavedState();
      System.out.println("Damn i'm back: " + state);
    }
  }
}
