/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class MacOSWidgetFactory implements AbstractWidgetFactory {

  @Override
  public Window createWindow() {
    MacOSWindow window = new MacOSWindow();
    return window;
  }

}
