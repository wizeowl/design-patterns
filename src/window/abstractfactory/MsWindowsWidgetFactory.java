/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class MsWindowsWidgetFactory implements AbstractWidgetFactory {

  @Override
  public Window createWindow() {
    MSWindow window = new MSWindow();
    return window;
  }
}
