/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public interface Window {

  public void setTitle(String text);

  public void repaint();
}
