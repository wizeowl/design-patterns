/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class WindowDecorator implements Window {

  protected Window window;

  public WindowDecorator(Window window) {
    this.window = window;
  }

  @Override
  public void draw() {
    window.draw();
  }

  @Override
  public String getDescription() {
    return window.getDescription();
  }
}
