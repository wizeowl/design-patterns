/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class HorizontalScrollbarDecorator extends WindowDecorator {

  public HorizontalScrollbarDecorator(Window window) {
    super(window);
  }

  @Override
  public void draw() {
    super.draw();
    drawHorizontalScrollbars();
  }

  @Override
  public String getDescription() {
    return super.getDescription() + "and Now with Horizontal scrollbars";
  }

  private void drawHorizontalScrollbars() {
    System.out.println("Here they are... Horizontal Scrollbars!!!");
  }
}
