/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class VerticalScrollbarDecorator extends WindowDecorator {

  public VerticalScrollbarDecorator(Window window) {
    super(window);
  }

  @Override
  public void draw() {
    super.draw();
    drawVerticalScrollbars();
  }

  @Override
  public String getDescription() {
    return super.getDescription() + " Now with scrollbars!!";
  }

  private void drawVerticalScrollbars() {
    System.out.println("Here they are.. Vertical Scrollbars!!!");
  }
}
