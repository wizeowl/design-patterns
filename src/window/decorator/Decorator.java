/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Decorator {

  public static void main(String[] args) {
    Window decorated = new HorizontalScrollbarDecorator(
            new VerticalScrollbarDecorator(new SimpleWindow()));
    System.out.println(decorated.getDescription());
    decorated.draw();
  }

}
