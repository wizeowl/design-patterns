/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package window.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class SimpleWindow implements Window {

  @Override
  public void draw() {
    System.out.println("A simple miserable window");
  }

  @Override
  public String getDescription() {
    return "simple window";
  }

}
