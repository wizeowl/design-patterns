/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.strategy;

/**
 *
 * @author nidhal.bentahar
 */
public class SortingContex {

  private ISort sorter = null;

  public void sortDouble(double[] list) {
    sorter.sort(list);
  }

  public ISort getSorter() {
    return sorter;
  }

  public void setSorter(ISort sorter) {
    this.sorter = sorter;
  }

}
