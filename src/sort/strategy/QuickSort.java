/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sort.strategy;

/**
 *
 * @author nidhal.bentahar
 */
public class QuickSort implements ISort {

  @Override
  public void sort(double[] list) {
    sort(list, 0, list.length - 1);
  }

  private void swap(double[] a, int i, int j) {
    double temp = a[i];
    a[i] = a[j];
    a[j] = temp;
  }

  private boolean smaller(double x, double y) {
    return (x < y);
  }

  private int part(double[] a, int left, int right) {
    int i = left;
    int j = right;
    while (true) {
      while (a[i] < a[right]) {
        i++;
      }
      while (smaller(a[right], a[--j])) {
        if (j == left) {
          break;
        }
      }
      if (i >= j) {
        break;
      }
      swap(a, i, j);
    }
    return i;
  }

  private void sort(double[] list, int right, int left) {
    if (right <= left) {
      return;
    }
    int i = part(list, left, right);
    sort(list, left, i - 1);
    sort(list, i + 1, right);
  }

}
