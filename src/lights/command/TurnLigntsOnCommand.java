/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lights.command;

/**
 *
 * @author nidhal.bentahar
 */
public class TurnLigntsOnCommand implements Command {

  private Light light;

  public TurnLigntsOnCommand(Light light) {
    this.light = light;
  }

  @Override
  public void execute() {
    light.turnOn();
  }

}
