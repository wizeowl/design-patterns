/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lights.command;

/**
 *
 * @author nidhal.bentahar
 */
public class CommandDemo {

  public static void main(String[] args) {
    Light light = new Light();
    Command sup = new TurnLigntsOnCommand(light);
    Command sdown = new TurnLightsOffCommand(light);
    Switch s = new Switch(sup, sdown);
    s.flipDown();
    s.flipUp();
    s.flipDown();
    s.flipUp();
    s.flipDown();
    s.flipUp();
    s.flipDown();
    s.flipUp();
    s.flipDown();
    s.flipUp();
  }
}
