/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lights.command;

/**
 *
 * @author nidhal.bentahar
 */
public class TurnLightsOffCommand implements Command {

  private Light light;

  public TurnLightsOffCommand(Light light) {
    this.light = light;
  }

  @Override
  public void execute() {
    light.turnOff();
  }

}
