/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lights.command;

/**
 *
 * @author nidhal.bentahar
 */
class Light {

  void turnOn() {
    System.out.println("LIGHT!!");
  }

  void turnOff() {
    System.out.println("NO LIGHT!!");
  }
  
}
