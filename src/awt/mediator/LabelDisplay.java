/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package awt.mediator;

import javax.swing.JLabel;

/**
 *
 * @author nidhal.bentahar
 */
class LabelDisplay extends JLabel {

  Mediator mediator;

  public LabelDisplay(Mediator mediator) {
    this.mediator = mediator;
    mediator.registerDisplay(this);
  }
}
