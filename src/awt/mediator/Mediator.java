/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package awt.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public class Mediator {

  ButtonView buttonView;
  ButtonBook buttonBook;
  ButtonSearch buttonSearch;
  LabelDisplay labelDisplay;

  void view() {
    buttonView.setEnabled(false);
    buttonBook.setEnabled(true);
    buttonSearch.setEnabled(true);
    labelDisplay.setText("Viewing...");
  }

  void book() {
    buttonView.setEnabled(true);
    buttonBook.setEnabled(false);
    buttonSearch.setEnabled(true);
    labelDisplay.setText("Booking...");
  }

  void search() {
    buttonView.setEnabled(true);
    buttonBook.setEnabled(true);
    buttonSearch.setEnabled(false);
    labelDisplay.setText("Searching...");
  }

  void registerView(ButtonView view) {
    buttonView = view;
  }

  void registerSearch(ButtonSearch search) {
    buttonSearch = search;
  }

  void registerBook(ButtonBook book) {
    buttonBook = book;
  }

  void registerDisplay(LabelDisplay label) {
    labelDisplay = label;
  }

}
