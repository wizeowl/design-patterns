/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package awt.mediator;

import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author nidhal.bentahar
 */
public class ButtonView extends JButton implements Command {

  Mediator mediator;

  public ButtonView(ActionListener listener, Mediator mediator) {
    super("View");
    addActionListener(listener);
    this.mediator = mediator;
    mediator.registerView(this);
  }

  @Override
  public void execute() {
    mediator.view();
  }

}
