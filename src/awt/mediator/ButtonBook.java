/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package awt.mediator;

import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author nidhal.bentahar
 */
class ButtonBook extends JButton implements Command {

  Mediator mediator;

  public ButtonBook(ActionListener listener, Mediator mediator) {
    super("Book");
    addActionListener(listener);
    this.mediator = mediator;
    mediator.registerBook(this);
  }

  @Override
  public void execute() {
    mediator.book();
  }

}
