/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package number.adapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class SortListAdpter implements Sorter {

  @Override
  public int[] sort(int[] numbers) {
    List<Integer> numberList = new ArrayList<Integer>();
    NumberSorter sorter = new NumberSorter();
    numberList = sorter.sort(numberList);
    int[] sortedNumbers = toArray(numberList);
    return sortedNumbers;
  }

  int[] toArray(List<Integer> list) {
    int[] ret = new int[list.size()];
    int i = 0;
    for (Iterator<Integer> it = list.iterator(); it.hasNext(); ret[i++] = it.next());
    return ret;
  }

}
