/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package number.adapter;

/**
 *
 * @author nidhal.bentahar
 */
public interface Sorter {

  public int[] sort(int[] numbers);
}
