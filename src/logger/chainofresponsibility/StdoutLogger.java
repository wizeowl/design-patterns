/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.chainofresponsibility;

/**
 *
 * @author nidhal.bentahar
 */
public class StdoutLogger extends Logger {

  public StdoutLogger(int mask) {
    this.mask = mask;
  }

  @Override
  protected void writeMessage(String message) {
    System.out.println("Writing to stdout");
  }

}
