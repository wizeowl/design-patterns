/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.chainofresponsibility;

/**
 *
 * @author nidhal.bentahar
 */
public class COR {

  private static Logger createChain() {
    Logger stdout = new StdoutLogger(Logger.DEBUG);
    Logger email = new EmailLogger(Logger.NOTICE);
    stdout.setNext(email);
    Logger strerr = new StderrLogger(Logger.ERR);
    email.setNext(strerr);
    return stdout;
  }

  public static void main(String[] args) {
    Logger chain = createChain();
    chain.message("Entering function y", Logger.DEBUG);
    chain.message("Allo Allo Terrim Terrim", Logger.NOTICE);
    chain.message("Nettalfa7", Logger.ERR);
  }
}
