/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.chainofresponsibility;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class Logger {

  public static int ERR = 3;
  public static int NOTICE = 5;
  public static int DEBUG = 7;
  protected int mask;
  protected Logger next;

  public void setNext(Logger next) {
    this.next = next;
  }

  public void message(String message, int priority) {
    if (priority <= mask) {
      writeMessage(message);
    }
    if (next != null) {
      next.message(message, priority);
    }
  }

  abstract protected void writeMessage(String message);
}
