/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Decorator {

  public static void main(String[] args) {
    LoggerFactory factory = new LoggerFactory();
    Logger logger = factory.getLogger(LoggerFactory.TYPE_CONSOLE_LOGGER);
    HtmlLogger htmlLogger = new HtmlLogger(logger);
    htmlLogger.log("Tena3amou biddibej");
  }
}
