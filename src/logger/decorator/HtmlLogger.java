/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class HtmlLogger extends LoggerDecorator {

  public HtmlLogger(Logger logger) {
    super(logger);
  }
  
  public void log(String msg){
    logger.log(makeHTML(msg));
  }

  private String makeHTML(String msg) {
    msg = "<html><body>" + "<b>" + msg + "</b>"
            + "</body></html>";
    return msg;
  }
}
