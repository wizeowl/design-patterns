/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class LoggerDecorator implements Logger {

  Logger logger;

  public LoggerDecorator(Logger logger) {
    super();
    this.logger = logger;
  }

  @Override
  public void log(String msg) {
    logger.log(msg);
  }

}
