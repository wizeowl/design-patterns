/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class LoggerFactory {

  public static final String TYPE_CONSOLE_LOGGER = "console";
  public static final String TYPE_FILE_LOGGER = "file";

  public Logger getLogger(String type) {
    if (TYPE_CONSOLE_LOGGER.equals(type)) {
      return new ConsoleLogger();
    } else {
      return new FileLogger();
    }
  }

}
