/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class EncryptLogger extends LoggerDecorator {

  public EncryptLogger(Logger logger) {
    super(logger);
  }

  @Override
  public void log(String msg) {
    logger.log(encrypt(msg));
  }

  private String encrypt(String msg) {
    String tmp = msg.substring(msg.length() - 1) + msg.substring(0,
            msg.length() - 1);
    return tmp;
  }
}
