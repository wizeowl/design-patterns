/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package name.state;

/**
 *
 * @author nidhal.bentahar
 */
public class StateContext {

  private State state;

  public StateContext() {
    setState(new StateA());
  }

  void setState(State state) {
    this.state = state;
  }

  public void writeName(String name) {
    this.state.writeName(this, name);
  }

}
