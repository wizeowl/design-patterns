/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package name.state;

/**
 *
 * @author nidhal.bentahar
 */
public class StateB implements State {

  int count = 0;

  @Override
  public void writeName(StateContext context, String name) {
    System.out.println(name.toUpperCase());
    if (++count > 1) {
      context.setState(new StateA());
    }
  }
  
}
