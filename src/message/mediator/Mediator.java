/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public interface Mediator {

  public void send(String message, Colleague colleague);
}
