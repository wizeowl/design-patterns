/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public class MobileColleague extends Colleague {

  public MobileColleague(Mediator mediator) {
    super(mediator);
  }

  @Override
  public void receive(String message) {
    System.out.println("Mobile received: " + message);
  }

}
