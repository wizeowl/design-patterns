/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message.mediator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class ApplicationMediator implements Mediator {

  private List<Colleague> colleagues;

  public ApplicationMediator() {
    colleagues = new ArrayList<Colleague>();
  }

  public void addColleague(Colleague colleague) {
    colleagues.add(colleague);
  }

  @Override
  public void send(String message, Colleague originator) {
    for (Colleague c : colleagues) {
      if (c != originator) {
        c.receive(message);
      }
    }
  }

}
