/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package message.mediator;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class Colleague {

  private Mediator mediator;

  public Colleague(Mediator mediator) {
    this.mediator = mediator;
  }

  public Mediator getMediator() {
    return mediator;
  }

  public void send(String message) {
    mediator.send(message, this);
  }

  public abstract void receive(String message);
}
