/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package job.command;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class Broker {

  private List<Command> produceRequests() {
    List<Command> queue = new ArrayList<Command>();
    queue.add(new DomesticEngineer());
    queue.add(new Politician());
    queue.add(new Programmer());
    return queue;
  }

  public void work() {
    for (Command c : produceRequests()) {
      c.execute();
    }
  }
}
