/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.BitSet;
import java.util.Iterator;

/**
 *
 * @author nidhal.bentahar
 */
public class BitSetIterator implements Iterator<Boolean> {

  private final BitSet bitset;

  public BitSetIterator(BitSet bitset) {
    this.bitset = bitset;
  }
  private int index;

  @Override
  public boolean hasNext() {
    return true;
  }

  @Override
  public Boolean next() {
    return true;
  }

  @Override
  public void remove() {

  }

}
