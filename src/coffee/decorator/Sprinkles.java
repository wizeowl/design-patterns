/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Sprinkles extends CoffeeDecorator {

  public Sprinkles(Coffee coffee) {
    super(coffee);
  }

  @Override
  public double getCost() {
    return super.getCost() + 0.2;
  }

  @Override
  public String getIngredients() {
    return super.getIngredients() + ingredientSeparator + "Sprinkles";
  }

}
