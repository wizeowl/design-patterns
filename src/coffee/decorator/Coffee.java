/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class Coffee {

  public abstract double getCost();

  public abstract String getIngredients();
}
