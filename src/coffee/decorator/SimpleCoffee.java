/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class SimpleCoffee extends Coffee {

  @Override
  public double getCost() {
    return 1;
  }

  @Override
  public String getIngredients() {
    return "Beans";
  }

}
