/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Decorator {

  public static void main(String[] args) {
    Coffee c = new SimpleCoffee();
    System.out.println(c.getCost());
    System.out.println(c.getIngredients());
    c = new Milk(c);
    System.out.println(c.getCost());
    System.out.println(c.getIngredients());
    c = new Whip(c);
    System.out.println(c.getCost());
    System.out.println(c.getIngredients());
    c = new Sprinkles(c);
    System.out.println(c.getCost());
    System.out.println(c.getIngredients());
  }
}
