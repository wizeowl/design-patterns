/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public class Whip extends CoffeeDecorator {

  public Whip(Coffee coffee) {
    super(coffee);
  }

  @Override
  public double getCost() {
    return super.getCost() + .7;
  }

  @Override
  public String getIngredients() {
    return super.getIngredients() + ingredientSeparator + "Whip";
  }

}
