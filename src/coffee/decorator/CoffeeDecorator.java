/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffee.decorator;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class CoffeeDecorator extends Coffee {

  protected final Coffee coffee;
  protected String ingredientSeparator = ",";

  public CoffeeDecorator(Coffee coffee) {
    this.coffee = coffee;
  }

  @Override
  public double getCost() {
    return coffee.getCost();
  }

  @Override
  public String getIngredients() {
    return coffee.getIngredients();
  }
}
