/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dog.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class Poodle implements Dog {

  @Override
  public void speak() {
    System.out.println("Poodle soup.. Miam Miam!!!");
  }

}
