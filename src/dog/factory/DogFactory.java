/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dog.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class DogFactory {

  public static Dog getDog(String criteria) {
    if (criteria.equals("small")) {
      return new Poodle();
    } else if (criteria.equals("big")) {
      return new Rottweiler();
    } else if (criteria.equals("working")) {
      return new SiberianHusky();
    }
    return null;
  }
}
