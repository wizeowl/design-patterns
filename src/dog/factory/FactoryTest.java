/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dog.factory;

/**
 *
 * @author nidhal.bentahar
 */
public class FactoryTest {

  public static void main(String[] args) {
    Dog dog = DogFactory.getDog("small");
    dog.speak();
    dog = DogFactory.getDog("big");
    dog.speak();
    dog = DogFactory.getDog("working");
    dog.speak();
  }
}
