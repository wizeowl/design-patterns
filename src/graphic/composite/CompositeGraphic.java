/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic.composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class CompositeGraphic implements Graphic {

  private List<Graphic> childGraphics = new ArrayList<Graphic>();

  @Override
  public void print() {
    System.out.println("CG");
    for (Graphic g : childGraphics) {
      g.print();
    }
  }

  public void add(Graphic g) {
    this.childGraphics.add(g);
  }

  public void remove(Graphic g) {
    this.childGraphics.remove(g);
  }

}
