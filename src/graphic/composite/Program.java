/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic.composite;

/**
 *
 * @author nidhal.bentahar
 */
public class Program {

  public static void main(String[] args) {
    Ellipse e1 = new Ellipse();
    Ellipse e2 = new Ellipse();
    Ellipse e3 = new Ellipse();
    Ellipse e4 = new Ellipse();

    CompositeGraphic g1 = new CompositeGraphic();
    CompositeGraphic g2 = new CompositeGraphic();
    CompositeGraphic g3 = new CompositeGraphic();
    g1.add(e1);
    g1.add(e2);
    g1.add(e3);
    g2.add(e4);
    g2.add(g1);
    g2.add(g3);
    g2.print();
  }
}
