/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.adapter;

/**
 *
 * @author nidhal.bentahar
 */
public class Line implements Shape {

  private LegacyLine adaptee = new LegacyLine();

  @Override
  public void draw(int x1, int y1, int x2, int y2) {
    adaptee.draw(x1, y1, x2, y2);
  }

}
