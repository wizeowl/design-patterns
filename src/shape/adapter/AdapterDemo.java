/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.adapter;

/**
 *
 * @author nidhal.bentahar
 */
public class AdapterDemo {

  public static void main(String[] args) {
    Shape[] shapes = {new Line(), new Rectangle()};
    int x1 = 10, y1 = 20, x2 = 30, y2 = 40;
    for (Shape s : shapes) {
      s.draw(x1, y1, x2, y2);
    }
  }
}
