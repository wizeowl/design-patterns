/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class CircleShape implements Shape {

  private double x, y, radius;
  private DrawingAPI drawingAPI;

  public CircleShape(double x, double y, double radius, DrawingAPI drawingAPI) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.drawingAPI = drawingAPI;
  }

  @Override
  public void draw() {
    drawingAPI.drawCircle(x, y, radius);
  }

  @Override
  public void resizeByPercentage(double pct) {
    radius *= pct;
  }

}
