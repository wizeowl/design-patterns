/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape.abstractfactory;

/**
 *
 * @author nidhal.bentahar
 */
public class AFDemo {

  public static void main(String[] args) {
    AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");

    Shape shape1 = shapeFactory.getShape("CIRCLE");
    Shape shape2 = shapeFactory.getShape("SQUARE");
    Shape shape3 = shapeFactory.getShape("RECTANGLE");
    shape1.draw();
    shape2.draw();
    shape3.draw();

    AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

    Color color1 = colorFactory.getColor("RED");
    Color color2 = colorFactory.getColor("GREEN");
    Color color3 = colorFactory.getColor("BLUE");
    color1.fill();
    color2.fill();
    color3.fill();
  }
}
