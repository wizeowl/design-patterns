/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stock.command;

/**
 *
 * @author nidhal.bentahar
 */
public class Command {

  public static void main(String[] args) {
    Stock stock = new Stock();
    BuyStock buy = new BuyStock(stock);
    SellStock sell = new SellStock(stock);
    Broker broker = new Broker();
    broker.takeOrder(buy);
    broker.takeOrder(sell);
    broker.placeOrders();
  }

}
