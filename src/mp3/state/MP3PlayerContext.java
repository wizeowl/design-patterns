/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mp3.state;

/**
 *
 * @author nidhal.bentahar
 */
public class MP3PlayerContext {

  private State state;

  private MP3PlayerContext(State state) {
    this.state = state;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public void play() {
    state.pressPlay(this);
  }
}
