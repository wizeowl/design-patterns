/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class Bike extends Vehicle {

  public Bike(Workshop workshop1, Workshop workshop2) {
    super(workshop1, workshop2);
  }

  @Override
  public void manifacture() {
    System.out.println("Bike");
    workshop1.work();
    workshop2.work();
  }

}
