/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public abstract class Vehicle {

  protected Workshop workshop1;
  protected Workshop workshop2;

  protected Vehicle(Workshop workshop1, Workshop workshop2) {
    this.workshop1 = workshop1;
    this.workshop2 = workshop2;
  }

  public abstract void manifacture();

}
