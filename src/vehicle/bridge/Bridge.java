/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle.bridge;

/**
 *
 * @author nidhal.bentahar
 */
public class Bridge {

  public static void main(String[] args) {
    Vehicle v1 = new Car(new Produce(), new Assemble());
    v1.manifacture();
    Vehicle v2 = new Bike(new Produce(), new Assemble());
    v2.manifacture();
  }

}
