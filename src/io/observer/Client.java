/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.observer;

/**
 *
 * @author nidhal.bentahar
 */
public class Client {
  
  public static void main(String[] args) {
    LogSubject subject = new LogSubject();
    IObserver o1 = new Observer();
    IObserver o2 = new Observer1();
    IObserver o3 = new Observer2();
    subject.attach(o1);
    subject.attach(o2);
    subject.attach(o3);
    subject.setState("I'm changing!!!");
    subject.detach(o3);
    subject.setState("Goodbye o3");
  }
}
