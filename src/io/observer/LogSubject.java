/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.observer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nidhal.bentahar
 */
public class LogSubject {

  private List<IObserver> observers = new ArrayList<IObserver>();
  private String state;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
    this.stateChanged();
  }

  public void attach(IObserver o) {
    observers.add(o);
  }

  public void detach(IObserver o) {
    observers.remove(o);
  }

  private void stateChanged() {
    for (IObserver o : observers) {
      o.update(getState());
    }
  }

}
