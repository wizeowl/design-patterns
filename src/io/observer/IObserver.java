/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.observer;

/**
 *
 * @author nidhal.bentahar
 */
public interface IObserver {

  void update(String state);
}
