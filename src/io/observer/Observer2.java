/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.observer;

/**
 *
 * @author nidhal.bentahar
 */
public class Observer2 implements IObserver {

  private String state;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  @Override
  public void update(String state) {
    setState(state);
    System.out.println("Observer2 has received update signal with the new state: " + getState());
  }
}
