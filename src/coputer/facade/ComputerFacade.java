/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coputer.facade;

/**
 *
 * @author nidhal.bentahar
 */
public class ComputerFacade {

  protected CPU processor;
  protected Memory ram;
  protected HardDrive hd;
  protected final long BOOT_ADDRESS = 0;
  protected final byte[] SECTOR_SIZE = null;

  public ComputerFacade() {
    this.processor = new CPU();
    this.ram = new Memory();
    this.hd = new HardDrive();
  }

  public void start() {
    processor.freeze();
    ram.load(BOOT_ADDRESS, SECTOR_SIZE);
    processor.jump(BOOT_ADDRESS);
    processor.execute();
  }
}
