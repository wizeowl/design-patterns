/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coputer.facade;

/**
 *
 * @author nidhal.bentahar
 */
public class CPU {

  public void freeze() {
    System.out.println("FREEZE... everybody clap your hands.");
  }

  public void jump(long position) {
    System.out.println("JUMP JUMP JUMP JUMP..");
  }

  public void execute() {
    System.out.println("Ready.. GO!");
  }
}
